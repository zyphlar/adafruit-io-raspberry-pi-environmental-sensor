print("Starting...")

import time
import Adafruit_DHT
from Adafruit_IO import Client, Feed, RequestError

ADAFRUIT_IO_KEY = 'YOUR_ADAFRUIT_IO_KEY_HERE'
ADAFRUIT_IO_USERNAME = 'YOUR_ADAFRUIT_IO_USERNAME_HERE'

LOOP_DELAY_MINS = 10
DHT_SENSOR = Adafruit_DHT.AM2302
DHT_PIN = 4

aio = Client(ADAFRUIT_IO_USERNAME, ADAFRUIT_IO_KEY)

try: # if we already have the feeds, assign them.
    temperature_feed = aio.feeds('temperature')
    humidity_feed = aio.feeds('humidity')
except RequestError: # if we don't, create and assign them.
    temperature_feed = aio.create_feed(Feed(name='temperature'))
    humidity_feed = aio.create_feed(Feed(name='humidity'))

while True:
    humidity, temperature_c = Adafruit_DHT.read_retry(DHT_SENSOR, DHT_PIN)
    temperature_f = temperature_c * (9 / 5) + 32

    if humidity is not None and temperature_c is not None:
        print("TempC={0:0.1f}*C TempF={1:0.1f}*F  Humidity={2:0.1f}%".format(temperature_c, temperature_f, humidity))

        print('sending data to adafruit io...')
        t_out = aio.send(temperature_feed.key, round(temperature_f,1))
        h_out = aio.send(humidity_feed.key, round(humidity,1))
        if t_out and h_out:
            print('data sent, sleeping for '+str(LOOP_DELAY_MINS)+' mins')
        else:
            print('problem sending data:')
            print(t_out)
            print(h_out)
    else:
        print("Failed to retrieve data from humidity sensor")

    time.sleep(LOOP_DELAY_MINS * 60)
