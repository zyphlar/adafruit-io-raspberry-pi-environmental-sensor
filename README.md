# Adafruit IO Raspberry Pi Environmental Sensor

Code based on https://pimylifeup.com/raspberry-pi-humidity-sensor-dht22/

## Code

If you are using the DHT22, DHT11, or the AM2302 you can switch the constant in the code as needed:
- `Adafruit_DHT.DH11`
- `Adafruit_DHT.DH22`
- `Adafruit_DHT.AM2302`

I'm using the AM2302 because it's all pre-wired.

Make sure you also change `ADAFRUIT_IO_KEY`, `ADAFRUIT_IO_USERNAME`, and `LOOP_DELAY_MINS` as desired. You can get Adafruit IO key/username info from your account at https://io.adafruit.com

## Wiring

To connect the sensor to the RasPi:

- If you're using DHT11 or DHT22, place a 10k resistor between Pin 1 and Pin 2 of the DHT sensor (a pull-up resistor from the data pin to the).
- Wire Pin 1 of the DHT22 to Physical Pin 1 (3v3) on the Pi
- Wire Pin 2 of the DHT22 to Physical Pin 7 (GPIO4) on the Pi
- Pin 3 of the DHT11/22 is left blank.
- Wire Pin 4 of the DHT22 (or Pin 3 of the AM2302) to Physical Pin 6 (GND) on the Pi

Physical pin 1 on the Pi is usually the far bottom-left corner of the GPIO header, as seen when the screenprinted text is right side up. Physical pin 2 is the top-left corner, and continuing in zigzag fashion with odd numbers on bottom and even numbers on top. See the PiMyLifeUp link above for a diagram.

## Installation

Install dependencies: 

  sudo apt-get update
  sudo apt-get upgrade
  sudo apt-get install python3-dev python3-pip
  sudo python3 -m pip install --upgrade pip setuptools wheel
  sudo pip3 install Adafruit_DHT

Copy the `environment.py` file to a folder of your choosing, make sure you edit it according to the Code section above.

## Running

Run it with `python3 environment.py`
